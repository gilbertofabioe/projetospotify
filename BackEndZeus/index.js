require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
var cors = require('cors');//cors
const app = express();

app.use(cors());//cors
app.use(
    express.urlencoded({
        extended: true
    })
)

app.use(express.json())

const playlistRoutes = require('./routes/playlistRoutes')
const usersRoutes = require('./routes/usersRoutes') 
const musicsRoutes = require('./routes/musicsRoutes')

app.use('/users', usersRoutes)

app.use('/playlists', playlistRoutes)

app.use('/musics', musicsRoutes)

app.get('/', (req,res)=>{
    res.json({message: 'Oi Express'})
})
const DB_USER = process.env.DB_USER
const DB_PASSWORD = encodeURIComponent(process.env.DB_PASSWORD)
//mongodb+srv://egyptont:<password>@spotifeiro.6bsqvsk.mongodb.net/test
mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@spotifeiro.6bsqvsk.mongodb.net/spotifeitodb?retryWrites=true&w=majority`).then(
    ()=>{
        console.log("Conectado ao MongoDB")
        app.listen(5001)
    }
).catch((err)=>console.log(err)

)

//app.listen(3000)

