const router = require('express').Router()
const Musics = require('../models/Musics')

//criar

router.post('/', async (req,res)=>{
    // Nome: String,
    // Endereco: String,
    const {Nome, Endereco} = req.body

    const musics = {
        Nome,
        Endereco
    }

    try{

        await Musics.create(musics)

        res.status(201).json({msg: `Música criada com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }

})

//procurar música

router.get('/search', async (req,res)=>{

    const {Nome} = req.query
    try{
        const musics = await Musics.find({ Nome: {$regex: '.*'+Nome+'.*'}})

        if(!musics){
            res.status(422).json({error: `Música ${Nome} não encontrada`})
            return
        }

        res.status(200).json(musics)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

router.get('/search', async (req,res)=>{

    const {Nome} = req.query
    try{
        const musics = await Musics.findOne({ Nome: Nome})

        if(!musics){
            res.status(422).json({error: `Música ${Nome} não encontrada`})
            return
        }

        res.status(200).json(Nome)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})



//ler
router.get('/', async (req,res)=>{
    try{
        
        const musics = await Musics.find()

        res.status(200). json(musics)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})


router.get('/:id', async (req,res)=>{
    const id = req.params.id

    try{
        
        const musics = await Musics.findOne({ _id: id})

        if(!musics){
            res.status(422).json({error: 'Musica não encontrada'})
            return
        }

        res.status(200).json(musics)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

   

router.patch('/:id', async (req,res)=>{

    const id = req.params.id

    const {Nome, Endereco} = req.body

    const musics = {
        Nome,
        Endereco
    }
    
    try{

        const updatedMusic = await Musics.updateOne({_id: id}, musics)

        if(updatedMusic.matchedCount === 0){
            res.status(422).json({error: 'Música não encontrada'})
            return
        }

        res.status(200).json(musics)

    }catch(error){
        res.status(500).json({error: error})
    }
})

router.delete('/:id', async (req,res)=>{
    
    const id = req.params.id

    const musics = await Musics.findOne({ _id: id})

    if(!musics){
        res.status(422).json({error: 'Música não encontrada'})
        return
    }

    try{

        await musics.deleteOne({ _id: id})

        res.status(200).json({message: `Música foi apagado com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }


})

module.exports = router;