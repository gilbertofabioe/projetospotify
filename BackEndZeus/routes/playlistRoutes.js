const router = require('express').Router()
const PlayLists = require('../models/Playlists')

//criar playlist

router.post('/', async (req,res)=>{
    // Nome: String,
    // Dono: String,
    // Musicas:[String],
    const {Nome, Dono, Musicas} = req.body

    const playlists = {
        Nome,
        Dono,
        Musicas,
    }

    try{

        await PlayLists.create(playlists)

        res.status(201).json({msg: `Playlist criada com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }

})

//listar playlists
router.get('/', async (req,res)=>{
    try{
        
        const playlists = await PlayLists.find()

        res.status(200). json(playlists)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

//detalhar playlist
router.get('/:id', async (req,res)=>{
    const id = req.params.id

    try{
        
        const playlists = await PlayLists.findOne({ _id: Dono})

        if(!playlists){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(playlists)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

   

router.patch('/:id', async (req,res)=>{

    const id = req.params.id

    const {Nome, Dono, Musicas} = req.body

    const playlists = {
        Nome,
        Dono,
        Musicas,
    }
    
    try{

        const updatedPlaylist = await PlayLists.updateOne({_id: id}, playlists)

        if(updatedPlaylist.matchedCount === 0){
            res.status(422).json({error: 'Playlist não encontrada'})
            return
        }

        res.status(200).json(playlists)

    }catch(error){
        res.status(500).json({error: error})
    }
})

router.delete('/:id', async (req,res)=>{
    
    const id = req.params.id

    const playlists = await PlayLists.findOne({ _id: id})

    if(!playlists){
        res.status(422).json({error: 'Playlist não encontrada'})
        return
    }

    try{

        await playlists.deleteOne({ _id: id})

        res.status(200).json({message: `Playlist foi apagado com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }


})

module.exports = router;