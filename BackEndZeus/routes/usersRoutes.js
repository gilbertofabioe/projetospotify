const router = require('express').Router()
const Users = require('../models/Users')

//cadastrar usuário

router.post('/', async (req,res)=>{
    // Email: String,
    // Username: String,
    // Password: String,
    // DataNasc: String, 
    // Genero: String,
    // PlayLists: {Number}, 
    const {Email, Username, Password, DataNasc, Genero} = req.body
    //const data = new Date().toLocaleDateString();

    // if(!valor){
    //     res.status(422).json({error: 'Valor é obrigatório'})
    //     return
    // }

    const users = {
        Email,
        Username,
        Password,
        DataNasc,
        Genero
    }

    try{

        await Users.create(users)

        res.status(201).json({msg: `Usuário inserido com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }

})

router.get('/', async (req,res)=>{
    // res.json({message: req.query})
    const {Email} = req.query

    try{
        
        const users = await Users.findOne({Email: Email})

        if(!users){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(users)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

//ler
router.get('/', async (req,res)=>{
    try{
        
        const users = await Users.find()

        res.status(200). json(users)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

//login
router.get('/login', async (req,res)=>{
    // res.json({message: req.query})
    const {Email} = req.query

    try{
        
        const users = await Users.findOne({Email: Email})

        if(!users){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(users)
        
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

router.get('/:id', async (req,res)=>{
    const id = req.params.id

    try{
        
        const users = await Users.findOne({ _id: id})

        if(!users){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(users)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

   
//editar user
router.patch('/:id', async (req,res)=>{

    const id = req.params.id

    const {Email, Username, Password, DataNasc, Genero} = req.body

    const users = {
        Email,
        Username,
        Password,
        DataNasc,
        Genero,
    }
    
    try{

        const updatedUsers = await Users.updateOne({_id: id}, users)

        if(updatedUsers.matchedCount === 0){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(users)

    }catch(error){
        res.status(500).json({error: error})
    }
})

router.delete('/:id', async (req,res)=>{
    
    const id = req.params.id

    const users = await Users.findOne({ _id: id})

    if(!users){
        res.status(422).json({error: 'Usuário não encontrado'})
        return
    }

    try{

        await Users.deleteOne({ _id: id})

        res.status(200).json({message: `Usuário foi apagado com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }


})

module.exports = router;