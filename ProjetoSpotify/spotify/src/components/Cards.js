import {Button, Card} from 'react-bootstrap';
import PlaylistIcon from '../asstes/playlisticon.png'
import {useState, useEffect} from 'react'
import Playlist_one from './Playlist_one';
import ReactAudioPlayer from 'react-audio-player';
import axe from '../asstes/axe.jpg'
import pagode from '../asstes/pagode.jpg'
import rock from '../asstes/rock.jpg'
import forro from '../asstes/forro.jpg'
import samba from '../asstes/samba.jpg'
import sertanejo from '../asstes/sertanejo.jpg'
import padrao from '../asstes/padrao.png'
import { FaPlayCircle } from "react-icons/fa";
import { Link, useNavigate } from 'react-router-dom';
//FaPlayCircle


function Cards(props) {
  const [togglePlaylist, SettogglePlaylist] = useState(false)
  useEffect(()=>{
    const loadData = async() =>{
      
    };
    loadData();
  },[])

  const handleToggle = () =>{
    SettogglePlaylist(!togglePlaylist);
  }

  const handleImg = () =>{
    if(props.lista.Nome == 'samba'){
      return samba
    }
    if(props.lista.Nome == 'axe'){
      return axe
    }
    if(props.lista.Nome == 'pagode'){
      return pagode
    }
    if(props.lista.Nome == 'sertanejo'){
      return sertanejo
    }
    if(props.lista.Nome == 'forro'){
      return forro
    }
    if(props.lista.Nome == 'rock'){
      return rock
    }
    if(props.lista.Nome == 'padrao'){
      return padrao
    }

  }

  return (
    <div className='Card'>
      <img src={handleImg()}/>
      <div className='Card-name'>
        <p>{props.lista.Nome}</p>
        <Link to={`/playlist/${props.lista._id}`}><FaPlayCircle /></Link>

        {togglePlaylist ? props.lista.musicas.map((musica)=>(<Playlist_one musica={musica}/>)) : <></>}
        
      </div>
    </div>
  );
}

export default Cards;